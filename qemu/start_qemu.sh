#! /bin/bash

qemu-system-x86_64 \
    -cpu host \
    -smp 2 \
    -machine type=pc,accel=kvm \
    -m 512M \
    -name "Debian Sid" \
    -drive file="images/image.qcow2",index=0,media=disk,format=qcow2,l2-cache-size=1M \
    -net nic,model=virtio \
    -net user \
    -vga virtio \
#    -soundhw all \
    -display gtk &>logs/image.log
