# ~/.bash_profile

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/scripts" ] ; then
    PATH="$HOME/scripts:$PATH"
fi

export PATH

export LC_CTYPE="tr_TR.UTF-8"
export LC_COLLATE="C" # sort hidden folders first
export NO_AT_BRIDGE=1

export TERMINAL="st-256color"
export EDITOR="vim"
export VISUAL="view"
export BROWSER="firefox"
export CM_LAUNCHER="dmenu"
export CM_ONLY_CLIPBOARD=1
export CM_MAX_CLIPS=100

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# less Colors for Man Pages
export LESS_TERMCAP_mb=$'\e[0;32m'        # begin blinking
export LESS_TERMCAP_md=$'\e[01;34m'       # begin bold
export LESS_TERMCAP_me=$'\e[0m'           # end mode
export LESS_TERMCAP_se=$'\e[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\e[01;33;246m'   # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\e[0m'           # end underline
export LESS_TERMCAP_us=$'\e[04;38;5;146m' # begin underline
