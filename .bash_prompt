# ~/.bash_prompt

# ANSI Escape Sequences
declare -A eseq
eseq=( [txtblk]='\e[0;30m'
       [txtred]='\e[0;31m'
       [txtgrn]='\e[0;32m'
       [txtylw]='\e[0;33m'
       [txtblu]='\e[0;34m'
       [txtpur]='\e[0;35m'
       [txtcyn]='\e[0;36m'
       [txtwht]='\e[0;37m'
       [bldblk]='\e[1;30m'
       [bldred]='\e[1;31m'
       [bldgrn]='\e[1;32m'
       [bldylw]='\e[1;33m'
       [bldblu]='\e[1;34m'
       [bldpur]='\e[1;35m'
       [bldcyn]='\e[1;36m'
       [bldwht]='\e[1;37m'
       [undblk]='\e[4;30m'
       [undred]='\e[4;31m'
       [undgrn]='\e[4;32m'
       [undylw]='\e[4;33m'
       [undblu]='\e[4;34m'
       [undpur]='\e[4;35m'
       [undcyn]='\e[4;36m'
       [undwht]='\e[4;37m'
       [bakblk]='\e[40m'
       [bakred]='\e[41m'
       [bakgrn]='\e[42m'
       [bakylw]='\e[43m'
       [bakblu]='\e[44m'
       [bakpur]='\e[45m'
       [bakcyn]='\e[46m'
       [bakwht]='\e[47m'
       [txtrst]='\e[0m'
       [txtbld]='\e[1m'
       [txtund]='\e[4m' )

exit_status () {
    local es=$?
    if [ $es -eq 0 ]; then
        printf "${eseq[bldwht]}"
    else
        printf "${eseq[bldred]}"
    fi
}

prompt_pwd() {
    dirs +0 | sed -e 's-\(\.*[^/]\)[^/]*/-\1/-g'
}

set_prompt () {
    # share history across all terminals
    history -a; history -c; history -r

    local titlebar
    case $TERM in
        xterm*|rxvt*|screen*|st*)
            titlebar='\[\033]0;\w\007\]'
            ;;
        *)
            titlebar=''
            ;;
    esac

    local temp
    temp=$(tty)
    local curtty=${temp:5}

    # > user@hostname (pts/_): [cwd] \n
    PS1="${titlebar}${eseq[txtrst]}\$(exit_status)> "
    PS1+="${eseq[txtrst]}${eseq[bldgrn]}\u${eseq[bldwht]}@${eseq[bldred]}\h"
    PS1+="${eseq[txtrst]} ${eseq[txtbld]}(${curtty}): "
    PS1+="${eseq[txtrst]}${eseq[bldwht]}[${eseq[bldblu]}\$(prompt_pwd)${eseq[bldwht]}]"
    PS1+="${eseq[txtrst]} \n"
}

PROMPT_COMMAND=set_prompt
