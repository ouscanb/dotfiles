" VIM configuration file

" make sure that vim-plug is installed
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync
endif

" load plugins
call plug#begin('~/.vim/plugged')
"
Plug 'tpope/vim-sensible'
Plug 'sheerun/vim-polyglot'
Plug 'justinmk/vim-syntax-extra'
Plug 'jlanzarotta/bufexplorer'
Plug 'tpope/vim-commentary'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/vim-easy-align'
Plug 'morhetz/gruvbox'
Plug 'justmao945/vim-clang'
"
call plug#end()



" settings
set encoding=utf-8
set lazyredraw
set hidden
set noswapfile
set number
set relativenumber
set splitright
set splitbelow
set hlsearch
set ignorecase
set smartcase
set nomodeline
set mouse=a
"
set smartindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set noshiftround
"
set backspace=indent,eol,start
set list
set listchars=tab:>.,nbsp:~,trail:.,extends:>,precedes:<
set matchpairs+=<:>
set wildignore=*.o,*~,*.pyc
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
"
set t_Co=256
colorscheme gruvbox
set background=dark
if &t_Co == 8
    highlight Comment   ctermfg=0   cterm=bold
    highlight LineNr    ctermfg=0   cterm=bold
endif
"
let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
"
set showmode
set showcmd
set cmdheight=1
set statusline=[%n]       "number of buffers
set statusline+=\ %t      "tail of the filename
"set statusline+=\ %h      "help file flag
set statusline+=\ %y      "filetype
set statusline+=\ %m      "modified flag
set statusline+=\ %r      "read only flag
set statusline+=\ %=      "left/right separator
set statusline+=\ %c,     "cursor column
set statusline+=\ %l/%L   "cursor line/total lines
set statusline+=\ %P      "percent through file
set statusline+=\ [%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=\ %{&ff}] "file format


" plugin settings
"
" gruvbox
let g:gruvbox_termcolors=16



" enhanced keyboard mappings
"
" change <leader> to ','
let mapleader=","
set      timeout timeoutlen=1500
nnoremap Q <nop>
noremap  n nzz
noremap  N Nzz
" jump back to the prev. location
noremap  <S-Tab> <C-o>
