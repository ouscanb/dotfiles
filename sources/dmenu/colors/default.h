static const char col_fg_n[] = "#bbbbbb";
static const char col_fg_s[] = "#eeeeee";
static const char col_fg_o[] = "#000000";
static const char col_bg_n[] = "#222222";
static const char col_bg_s[] = "#005577";
static const char col_bg_o[] = "#00ffff";

static const char *colors[SchemeLast][2] = {
	/*               fg         bg       */
	[SchemeNorm] = { col_fg_n, col_bg_n },
	[SchemeSel]  = { col_fg_s, col_bg_s },
	[SchemeOut]  = { col_fg_o, col_bg_o },
};
