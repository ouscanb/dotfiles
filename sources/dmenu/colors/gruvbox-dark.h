static const char col_fg_n[] = "#EBDBB2";
static const char col_fg_s[] = "#F9F5D7";
static const char col_fg_o[] = "#EBDBB2";
static const char col_bg_n[] = "#1D2021";
static const char col_bg_s[] = "#504945";
static const char col_bg_o[] = "#32302F";

static const char *colors[SchemeLast][2] = {
	/*               fg        bg      */
	[SchemeNorm] = { col_fg_n, col_bg_n },
	[SchemeSel]  = { col_fg_s, col_bg_s },
	[SchemeOut]  = { col_fg_o, col_bg_o },
};
