# ~/.bashrc: executed by bash(1) for non-login shells.

case $- in
    *i*) ;;
    *) return;;
esac

unset HISTFILESIZE
HISTCONTROL=ignoreboth:erasedups
HISTSIZE=1000
HISTFILESIZE=
HISTIGNORE='&:clear:ls:cd:[bf]g:exit:[ t\]*'

set -o noclobber
shopt -s autocd
#shopt -s cdable_vars
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s globstar
shopt -s gnu_errfmt
shopt -s histappend
shopt -s hostcomplete
shopt -s nocaseglob
shopt -s progcomp

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.bash_prompt ]; then
    . ~/.bash_prompt
fi

if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

if [ "${TERM:-none}" = "linux" ]; then
    [ -f ~/.gruvbox.sh ] && ~/.gruvbox.sh
fi
